using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Core;

namespace ChatServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream {get; private set;}
        string userName;
        TcpClient client;
        ServerObject server; // объект сервера
 
        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }
 
        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                string message = String.Empty;
                // получаем имя пользователя
                var objMessage = GetMessageFromStream();
                if (objMessage.TypeMessage == TypeMessage.Error)
                {
                    message = objMessage.GetErrorIfExist();
                } else
                {
                    userName = objMessage.Text;
                    message = userName + " вошел в чат";
                }
                
                // посылаем сообщение о входе в чат всем подключенным пользователям
                server.BroadcastMessage(message, this.Id);
                Console.WriteLine(message);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        objMessage = GetMessageFromStream();
                        if (objMessage.TypeMessage == TypeMessage.Error)
                        {
                            message = objMessage.GetErrorIfExist();
                        } else
                        {
                            message = String.Format("{0}: {1}", userName, objMessage.Text);
                        }

                        Console.WriteLine(message);
                        server.BroadcastMessage(message, this.Id);
                    }
                    catch
                    {
                        message = String.Format("{0}: покинул чат", userName);
                        Console.WriteLine(message);
                        server.BroadcastMessage(message, this.Id);
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }
 
        // чтение входящего сообщения и преобразование в строку
        private CommunicationMessage GetMessageFromStream()
        {
            var result = new List<byte>();
            byte[] buffer = new byte[2048]; // буфер для получаемых данных
            int bytes = 0;
            do
            {
                bytes = Stream.Read(buffer, 0, buffer.Length);
                result.AddRange(buffer);
            }
            while (Stream.DataAvailable);
            var objMessage = result.ToArray().DeSerialize();
 
            return objMessage;
        }
 
        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}