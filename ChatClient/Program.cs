﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Microsoft.Extensions.Configuration;

namespace ChatClient
{
    class Program
    {
        static string userName;
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json");

            var config = builder.Build();

            var port = int.Parse(config["port"]);
            var host = config["host"];
                
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            Communication _communication = null;
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток
                _communication = new Communication(stream, client, userName);
                
                _communication.SendMessageToStream(userName);
                
                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(_communication.ReceiveMessageAndShow));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", userName);
                _communication.HandleSendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _communication?.Disconnect();
            }
        }
    }
}