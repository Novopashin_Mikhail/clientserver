using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Core;

namespace ChatClient
{
    public class Communication
    {
        private NetworkStream _stream;
        private TcpClient _client;
        private string _userName;
        public Communication()
        {
        }
        
        public Communication(NetworkStream stream, TcpClient client, string userName)
        {
            _stream = stream;
            _client = client;
            _userName = userName;
        }
        
        public void HandleSendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                string text = Console.ReadLine();
                SendMessageToStream(text);
            }
        }

        public void SendMessageToStream(string text = default)
        {
            var message = new CommunicationMessage(text, _userName);
            var data = message.Serialize();
            _stream.Write(data, 0, data.Length);
        }

        // получение сообщений
        public void ReceiveMessageAndShow()
        {
            while (true)
            {
                try
                {
                    var result = new List<byte>();
                    byte[] buffer = new byte[2048]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = _stream.Read(buffer, 0, buffer.Length);
                        result.AddRange(buffer);
                    } while (_stream.DataAvailable);
                    
                    var objMessage = result.ToArray().DeSerialize();

                    string message = string.Empty;
                    if (objMessage.TypeMessage == TypeMessage.Error)
                    {
                        message = $"{objMessage?.GetErrorIfExist()}";
                    }
                    else
                    {
                        message = $"{objMessage?.Text}";
                    }

                    Console.WriteLine(message);
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }
        
        public void Disconnect()
        {
            _stream?.Close(); //отключение потока
            _client?.Close(); //отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}