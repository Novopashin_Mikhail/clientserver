﻿using System;
using System.Runtime.Serialization;

namespace Core
{
    [Serializable]
    public class CommunicationMessage : ISerializable, IError
    {
        public CommunicationMessage(){}
        public CommunicationMessage(string text, string clientName)
        {
            ClientName = clientName;
            Text = text;
        }

        public CommunicationMessage(SerializationInfo info, StreamingContext context)
        {
            ClientName = (string) info.GetValue("ClientName", typeof(string));
            Text = (string) info.GetValue("Text", typeof(string));
            TypeMessage = (TypeMessage) info.GetValue("TypeMessage", typeof(TypeMessage));
        }
        public string ClientName { get; set; }
        public string Text { get; set; }
        public TypeMessage TypeMessage { get; set; }
        [NonSerialized] private string _error;
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ClientName", ClientName, typeof(string));
            info.AddValue("Text", Text, typeof(string));
            info.AddValue("TypeMessage", TypeMessage, typeof(TypeMessage));
        }

        public string GetErrorIfExist()
        {
            return string.IsNullOrEmpty(_error) ? null : _error;
        }

        public void SetError(string error)
        {
            TypeMessage = TypeMessage.Error;
            _error = error;
        } 
    }

    public enum TypeMessage
    {
        Info,
        Error
    }
}