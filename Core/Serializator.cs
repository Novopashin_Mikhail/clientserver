using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Core
{
    public class Serializator
    {
        private static readonly BinaryFormatter BinaryFormatter = new BinaryFormatter();
        public static byte[] Serialize(object obj)
        {
            var memoryStream = new MemoryStream();
            var binaryFormatter = new BinaryFormatter();
            BinaryFormatter.Serialize(memoryStream, obj);
            memoryStream.Position = 0;
            return StreamToByteArray(memoryStream);
        }

        private static byte[] StreamToByteArray(Stream stream)
        {
            byte[] buffer = new byte[16*1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}