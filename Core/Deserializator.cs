using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Core
{
    public class Deserializator
    {
        private static readonly BinaryFormatter BinaryFormatter = new BinaryFormatter();
        public static T Deserialize<T>(byte[] byteArray) where T: IError, new()
        {
            T obj = default;
            try
            {
                var stream = new MemoryStream(byteArray);
                stream.Position = 0;
                obj = (T)BinaryFormatter.Deserialize(stream);
            }
            catch (Exception e)
            {
                obj = new T();
                obj.SetError($"Ошибка десереализации");
            }

            return obj;
        }
    }
}