namespace Core
{
    public interface IError
    {
        void SetError(string error);
    }
}