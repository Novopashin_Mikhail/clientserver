namespace Core
{
    public static class Extensions
    {
        
        public static byte[] Serialize(this CommunicationMessage message)
         {
             return Serializator.Serialize(message);
         }
        
        public static CommunicationMessage DeSerialize(this byte[] byteArray)
        {
            return Deserializator.Deserialize<CommunicationMessage>(byteArray);
        }
    }
}